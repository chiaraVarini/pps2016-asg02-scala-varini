package utils

import java.awt.event.KeyEvent.{VK_LEFT, VK_RIGHT, VK_UP}
import java.awt.event.{KeyEvent, KeyListener}

import game.controller.Controller

/**
  * Created by chiaravarini on 04/04/17.
  */


class Keyboard extends KeyListener{


  override def keyPressed(e: KeyEvent): Unit = e.getKeyCode match {

    case VK_RIGHT => Controller.backgroundShiftRight(true)
    case VK_LEFT => Controller.backgroundShiftRight(false)
    case VK_UP => Controller.mainCharacterJump()
    case _ =>
  }

  override def keyTyped(e: KeyEvent): Unit = {}
  override def keyReleased(e: KeyEvent): Unit = Controller.stopBackgroundShift()
}
