package utils

import java.awt.Image
import java.net.URL
import javafx.scene.media.{Media, MediaPlayer}
import javax.sound.sampled.AudioSystem
import javax.swing.ImageIcon


/**
  * Created by chiaravarini on 04/04/17.
  */

object Utils {

  private var mediaPlayer: MediaPlayer = null

  def getResource(path: String): URL = Utils.getClass.getResource(path)

  def getImage(path: String): Image = new ImageIcon(getResource(path)).getImage

  def playSound(sound: String):Unit = {
    val clip = AudioSystem.getClip
    clip.open(AudioSystem.getAudioInputStream(getResource(sound)))
    clip.start()
  }

  def backgroundSoundPlay(song: String):Unit = {

    var hit = new Media(getResource(song).toURI.toString)
    mediaPlayer = new MediaPlayer(hit)
    mediaPlayer.play()

  }

  def stopSong():Unit = if (mediaPlayer!=null) mediaPlayer.stop()

}
