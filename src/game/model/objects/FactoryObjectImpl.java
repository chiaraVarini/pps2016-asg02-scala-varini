package game.model.objects;

/**
 * Created by chiaravarini on 18/03/17.
 */
public class FactoryObjectImpl implements FactoryObject {

    @Override
    public GameObject createBlock(final int x, final int y) {
        return new Block(x,y);
    }

    @Override
    public GameObject createTunnel(final int x, final int y) {
        return new Tunnel(x,y);
    }

    @Override
    public Piece createPiece(final int x, final int y) {
        return new PieceImpl(x,y);
    }
}
