package game.model.objects;

/**
 * Created by chiaravarini on 18/03/17.
 */
public interface FactoryObject {

    GameObject createBlock(final int x, final int y);
    GameObject createTunnel(final int x, final int y);
    Piece createPiece(final int x, final int y);
}
