package game.model.objects;

import java.awt.*;

/**
 * Represent a piece in the game
 * Created by chiaravarini on 15/03/17.
 */
public interface Piece extends GameObject, Runnable {

    /**
     * @return image movement of piece
     */
    Image imageOnMovement();

    void stopMove();

    int getMoney();
}
