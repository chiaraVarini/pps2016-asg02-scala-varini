package game.model.characters.enemies;

import game.model.characters.BasicCharacter;
import game.model.characters.Character;
import game.model.entities.Entity;
import utils.HitPosition;



/**
 * Created by chiaravarini on 15/03/17.
 */
public abstract class EnemyImpl extends BasicCharacter implements Runnable, Character {

    private static final int PAUSE = 15;


    public EnemyImpl(int X, int Y, int width, int height) {
        super(X, Y, width, height);
        super.setToRight(true);
        super.setMoving(true);

        Thread t = new Thread(this);
        t.start();
    }

    @Override
    public void move(int Xposition, int Yposition) {
        super.setX(super.getX() + Xposition);
        super.setY(Yposition);
    }

    @Override
    public void run() {
        while (this.isAlive()) {

            if(isToRight()){
                this.move(1, getY());
            }else {
                this.move(-1, getY());
            }

            try {
                Thread.sleep(PAUSE);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public boolean contact(final Entity entity){
        if (this.hit(entity, HitPosition.HEAD) && this.isToRight()) {
            this.setToRight(false);
        } else if (this.hit(entity, HitPosition.BACK) && !this.isToRight()) {
            this.setToRight(true);
        }
        return false;
    }

}


