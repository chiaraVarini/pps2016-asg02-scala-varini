package game.model.characters.enemies;

public class Turtle extends EnemyImpl {

    private static final int WIDTH = 43;
    private static final int HEIGHT = 50;

    public Turtle(int X, int Y) {
        super(X, Y, WIDTH, HEIGHT);
    }

}
