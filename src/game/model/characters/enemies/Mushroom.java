package game.model.characters.enemies;

public class Mushroom extends EnemyImpl {

    private static final int WIDTH = 27;
    private static final int HEIGHT = 30;

    public Mushroom(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
    }

}
