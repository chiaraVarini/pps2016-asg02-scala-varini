package game.model.characters;

import game.controller.Controller;
import game.model.entities.Entity;
import game.model.objects.GameObject;
import game.model.objects.Piece;
import utils.HitPosition;


public class Mario extends BasicCharacter implements MainCharacter {

    private static final int MARIO_OFFSET_Y_INITIAL = 243;
    private static final int FLOOR_OFFSET_Y_INITIAL = 293;
    private static final int WIDTH = 28;
    private static final int HEIGHT = 50;

    private boolean jumping;
    private int score;

    public Mario(int x, int y) {
        super(x, y, WIDTH, HEIGHT);
        this.jumping = false;
        score = 0;
    }

    @Override
    public boolean isJumping() {
        return jumping;
    }

    @Override
    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    @Override
    public boolean contact(Entity entity) {
        if(entity instanceof Piece){
            return contactPiece((Piece)entity);

        } else if (entity instanceof GameObject){
            return contactObject((GameObject)entity);

        } else {
            return contactCharacter((Character) entity);
        }
    }

    @Override
    public void setScore(int score){
        this.score = score;
    }

    @Override
    public int getScore(){
        return this.score;
    }

    private boolean contactObject(GameObject obj) {


        if (this.hit(obj, HitPosition.HEAD) && this.isToRight() || this.hit(obj, HitPosition.BACK) && !this.isToRight()) {
            Controller.setMovBackgroundGui(0);
            this.setMoving(false);
        }

        if (this.hit(obj, HitPosition.BELOW) && this.jumping) {
            Controller.setFloorOffsetYBackgroundGui(obj.getY());

        } else if (!this.hit(obj,  HitPosition.BELOW)) {
            Controller.setFloorOffsetYBackgroundGui(FLOOR_OFFSET_Y_INITIAL);

            if (!this.jumping) {
                this.setY(MARIO_OFFSET_Y_INITIAL);
            }

            if (hit(obj, HitPosition.ABOVE)) {
                Controller.setHeightLimitBackgroundGui(obj.getY() + obj.getHeight()); // the new sky goes below the object

            } else if (!this.hit(obj, HitPosition.ABOVE) && !this.jumping) {
                Controller.setHeightLimitBackgroundGui(0); // initial sky
            }
        }
        return false;
    }

    private boolean contactPiece(Piece piece) {
        return  (this.hit(piece, HitPosition.BACK) ||
                this.hit(piece, HitPosition.ABOVE) ||
                this.hit(piece, HitPosition.HEAD)  ||
                this.hit(piece, HitPosition.BACK));
    }

    private boolean contactCharacter(Character pers) {
        if (this.hit(pers, HitPosition.HEAD) || this.hit(pers, HitPosition.BACK)) {
            if (pers.isAlive()) {
                this.setMoving(false);
                this.setAlive(false);
            } else {
                this.setAlive(true);
            }
        } else if (this.hit(pers, HitPosition.BELOW)) {
            pers.setMoving(false);
            pers.setAlive(false);
        }
        return false;
    }
}
