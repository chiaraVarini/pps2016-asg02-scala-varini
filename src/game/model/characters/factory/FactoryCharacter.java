package game.model.characters.factory;

import game.model.characters.Character;
import game.model.characters.MainCharacter;

/**
 * Created by chiaravarini on 17/03/17.
 */
public interface FactoryCharacter {

    MainCharacter createMario();
    Character createMushroom(int x, int y);
    Character createTurtle(int x, int y);
}
