package game.controller

import game.view.BackgroundGui
import game.view.EndPanel
import game.view.GameFrame
import javax.swing._

/**
  * Conrtoller that menaged the repaint of gui
  * Created by chiaravarini on 17/03/17.
  */

object ControllerGui {

  private var backgroundGui: BackgroundGui = null

  def setGui(gameFrame: GameFrame) = this.backgroundGui = gameFrame.getBackgroundGui

  def shiftBackgorundToRight() {
    if (backgroundGui.getxPos == -1) {
      backgroundGui.setxPos(0)
      backgroundGui.setBackground1PosX(-50)
      backgroundGui.setBackground2PosX(750)
    }
    getBackgroundGui.setMov(1)
  }

  def shiftBackgorundToLeft() {
    if (backgroundGui.getxPos == 4601) {
      backgroundGui.setxPos(4600)
      backgroundGui.setBackground1PosX(-50)
      backgroundGui.setBackground2PosX(750)
    }
    getBackgroundGui.setMov(-1)
  }

  def stopShift() = backgroundGui.setMov(0)

  def getBackgroundGui: BackgroundGui =  this.backgroundGui

}