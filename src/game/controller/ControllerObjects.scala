package game.controller

import game.model.objects._

import scala.collection.JavaConverters._
import java.util.Collections

import scala.collection.mutable.ListBuffer

/**
  * Controller of GameObjects
  * that managed the change of position of them
  */


object ControllerObjects{

  val factory: FactoryObject = new FactoryObjectImpl
  private var objects: List[GameObject]= initObject()
  private var pieces: List[Piece]= initPiece()

  def restart(): Unit ={
    objects = initObject()
    pieces = initPiece()
  }

  def moveObject(position: Int) {
    objects.foreach(o => o.move(position, o.getY()))
    pieces.foreach(p => p.move(position, p.getY()))
  }

  def getObjects: List[GameObject] = objects

  def getPieces: List[Piece] = pieces

  def getPiece(index: Int): Option[Piece] = if(index<pieces.size) Option(pieces(index)) else Option.empty

  def removePiece(index: Int) = pieces = pieces.take(index) ++ pieces.drop(index+1)

  private def initPiece(): List[Piece] ={
    List[Piece](factory.createPiece(402, 145),
                factory.createPiece(1202, 140),
                factory.createPiece(1272, 95),
                factory.createPiece(1342, 40),
                factory.createPiece(1650, 145),
                factory.createPiece(2650, 145),
                factory.createPiece(3000, 135),
                factory.createPiece(3400, 125),
                factory.createPiece(4200, 145),
                factory.createPiece(4600, 40))
  }

  private def initObject(): List[GameObject] = {
   List[GameObject] (factory.createTunnel(600, 230), factory.createTunnel(1000, 230),
                     factory.createTunnel(1600, 230),factory.createTunnel(1900, 230),
                     factory.createTunnel(2500, 230),factory.createTunnel(3000, 230),
                     factory.createTunnel(3800, 230),factory.createTunnel(4500, 230),
                     factory.createBlock(400, 180),  factory.createBlock(1200, 180),
                     factory.createBlock(1270, 170), factory.createBlock(1340, 160),
                     factory.createBlock(2000, 180), factory.createBlock(2600, 160),
                     factory.createBlock(2650, 180), factory.createBlock(3500, 160),
                     factory.createBlock(3550, 140), factory.createBlock(4000, 170),
                     factory.createBlock(4200, 200), factory.createBlock(4300, 210))
  }
}