package game.controller

import javafx.application.Application
import javax.swing.JLabel

import game.model.characters._
import game.model.objects._
import game.view.character.EnemyGui
import game.view._
import utils.Res
import utils.Utils

import scala.collection.immutable.Map
import scala.collection.immutable.List


/**
  * Controller of all application
  * Created by chiaravarini on 17/03/17.
  */

object Controller {

  private var frame: GameFrame = new GameFrame()
  private val THREAD_PAUSE = 3
  var finishGame = false

  def startGame () {

    ControllerGui.setGui(frame)
    ControllerObjects.getPieces.foreach(p => new Thread(p).start())

    Utils.backgroundSoundPlay(Res.AUDIO_LEVEL)

    new Thread(new Runnable {

      def run(): Unit = {

        while(ControllerCharacter.getMainCharacter.isAlive && !finishGame){

          ControllerGui.getBackgroundGui.repaint()

          try {
            Thread.sleep(THREAD_PAUSE)
          } catch {
            case e => e.printStackTrace()
          }
        }
      }
    }).start()

  }

  def restart(): Unit = {
    finishGame = false
    ControllerObjects.restart()
    ControllerCharacter.restart()
    frame.restart()
    startGame()
  }

  def mainCharacterJump () {
    ControllerCharacter.setMainCharacterJumping (true)
    Utils.playSound (Res.AUDIO_JUMP)
  }

  def mainCharacterToRight: Boolean = ControllerCharacter.getMainCharacter.isToRight

  def setMainCharacterJump (jump: Boolean) = ControllerCharacter.setMainCharacterJumping (jump)

  def getMainCharacterDimension (dimension: String): Int = {
    if (dimension == "height") {
      return ControllerCharacter.getMainCharacter.getHeight
    }
    else if (dimension == "width") {
      return ControllerCharacter.getMainCharacter.getWidth
    }

    Integer.MIN_VALUE
  }

  def getMainCharacterPositon (position: String): Int = position match {
    case "x" => ControllerCharacter.getMainCharacter.getX
    case "y" => ControllerCharacter.getMainCharacter.getY
    case _ => Integer.MIN_VALUE
  }

  def getObjects: List[GameObject] = ControllerObjects.getObjects

  def getPieces: List[Piece] =  ControllerObjects.getPieces

  def getEnemies: Map[Character, EnemyGui] =  ControllerCharacter.getEnemies

  def getMainCharacter: MainCharacter =  ControllerCharacter.getMainCharacter

  def setMainCharacterPosition (position: String, value: Int) = position match{
    case "x" => ControllerCharacter.getMainCharacter.setX (value)
    case "y" => ControllerCharacter.getMainCharacter.setY (value)
    case _ =>
  }

  def modifyPosition () =  ControllerCharacter.modifyPosition()

  def backgroundShiftRight (right: Boolean) {
    if (right) ControllerGui.shiftBackgorundToRight ()
    else ControllerGui.shiftBackgorundToLeft ()

    ControllerCharacter.moveRightMainCharacter (right)
  }

  def stopBackgroundShift () {
    ControllerCharacter.stopWalk ()
    ControllerGui.stopShift ()
  }

  def moveCamera (position: Int) {
    ControllerObjects.moveObject (position)
    ControllerCharacter.moveEnemy (position)
  }

  def setMovBackgroundGui (mov: Int) = ControllerGui.getBackgroundGui.setMov (mov)

  def setFloorOffsetYBackgroundGui (floorOffset: Int) = ControllerGui.getBackgroundGui.setFloorOffsetY (floorOffset)

  def setHeightLimitBackgroundGui (heightLimit: Int) = ControllerGui.getBackgroundGui.setHeightLimit (heightLimit)

  def getHeightLimit: Int = ControllerGui.getBackgroundGui.getHeightLimit

  def getFloorOffsetY: Int = ControllerGui.getBackgroundGui.getFloorOffsetY


  def endGame (endGamePhrase: String ) {

    finishGame = true
    ControllerCharacter.getEnemies.keySet.foreach(e => e.setAlive(false))
    ControllerObjects.getPieces.foreach(p => p.stopMove())

    frame.getContentPane.add(new EndPanel(endGamePhrase))
    frame.revalidate ()
    frame.repaint ()
  }

  def finishLevel(): Unit ={
    Utils.stopSong()
    Utils.playSound(Res.AUDIO_STAGE_CLEAR)
    Thread.sleep(4000)
    endGame("YOU WIN !!!")
  }
}