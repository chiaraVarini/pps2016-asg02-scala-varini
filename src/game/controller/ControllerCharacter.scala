package game.controller

import scala.collection.immutable.Map
import game.model.characters.Character
import game.model.characters.factory.FactoryCharacter
import game.model.characters.factory.FactoryCharacterImpl
import game.model.characters._
import game.view.character.EnemyGui
import game.view.character.MushroomGui
import game.view.character.TurtleGui
import utils.Res
import utils.Utils


/**
  * Controller of Charcters
  * that managed the change of position of them
  */

@SuppressWarnings(Array("serial")) object ControllerCharacter  {

  val factory: FactoryCharacter = new FactoryCharacterImpl

  var mario: MainCharacter = factory.createMario
  var enemies: Map[Character, EnemyGui] = initEnemies()

  def getMainCharacter: MainCharacter =  mario

  def getEnemies: Map[Character, EnemyGui] = enemies

  def restart(){
    mario = factory.createMario
    enemies = initEnemies()
  }

  def moveRightMainCharacter(right: Boolean) {
    getMainCharacter.setMoving(true)
    getMainCharacter.setToRight(right)
  }

  def setMainCharacterJumping(jump: Boolean) = getMainCharacter.setJumping(jump)

  def stopWalk() = getMainCharacter.setMoving(false)

  def moveEnemy(position: Int) = enemies.keySet.foreach( e => e.move( if(e.isToRight()) 1 else - 1, e.getY()) )

  def modifyPosition() {

    ControllerObjects.getObjects.foreach( obj => {
      if (mario.isNearby(obj)) mario.contact(obj)
      enemies.keySet.foreach(enemy => if (enemy.isNearby(obj)) enemy.contact(obj))
    })

    for (i<-0 until ControllerObjects.getPieces.size){

      if (ControllerObjects.getPiece(i).nonEmpty && mario.contact(ControllerObjects.getPiece(i).get)) {
        Utils.playSound(Res.AUDIO_MONEY)
        mario.setScore(mario.getScore+ControllerObjects.getPieces(i).getMoney)
        ControllerObjects.getPieces(i).stopMove()
        ControllerGui.getBackgroundGui.setScore(mario.getScore)
        ControllerObjects.removePiece(i)
      }
    }

    this.enemies.keySet.foreach(enemy => {
      if (enemy.isAlive){

        enemies.keySet.foreach(otherCharacter => {
          if (!enemy.equals(otherCharacter))
            if (enemy.isNearby(otherCharacter)) enemy.contact(otherCharacter)
        })

        if (mario.isNearby(enemy)) mario.contact(enemy)
      }
    })

    if(!mario.isAlive){
      Utils.stopSong()
      Utils.playSound(Res.AUDIO_DIE)
      Controller.endGame(" GAME OVER ")
    }
  }

  private def initEnemies(): Map[Character, EnemyGui] = {
    Map(factory.createMushroom(800, 263)-> new MushroomGui,
      /*factory.createMushroom(850, 263)-> new MushroomGui,
      /factory.createMushroom(900, 263)-> new MushroomGui,
      factory.createTurtle(950, 243)-> new TurtleGui,
      factory.createTurtle(1000, 243)-> new TurtleGui,*/
      factory.createTurtle(1110, 243)-> new TurtleGui)
  }
}