package game.view;

import game.controller.Controller;

import javax.swing.*;
import java.awt.*;

import static java.lang.System.exit;

/**
 * Created by chiaravarini on 18/03/17.
 */
public class EndPanel extends JPanel {

    public EndPanel(String endGamePhrase){
       setOpaque(false);

        setLayout(new BorderLayout());
        JLabel label = new JLabel(endGamePhrase);
        label.setForeground(Color.white);

        Font labelFont = label.getFont();
        label.setFont(new Font(labelFont.getName(), Font.BOLD, 50));

        JPanel p = new JPanel();
        p.setOpaque(false);
        p.add(label);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setOpaque(false);

        JButton restart = new JButton("Retry");
        restart.addActionListener(e-> Controller.restart());
        buttonPanel.add(restart);

        JButton quit = new JButton("Quit");
        quit.addActionListener(e->exit(1));
        buttonPanel.add(quit);

        this.add(p, BorderLayout.NORTH);
        this.add(buttonPanel, BorderLayout.CENTER);
    }
}
