package game.view;

import utils.Keyboard;

import java.awt.*;
import javax.swing.*;

public class GameFrame extends JFrame {

    private static final int WINDOW_WIDTH = Toolkit.getDefaultToolkit().getScreenSize().width / 2;
    private static final int WINDOW_HEIGHT = (int) (Toolkit.getDefaultToolkit().getScreenSize().height / 2.5);
    private static final String WINDOW_TITLE = "Super Mario";
    private BackgroundGui backgroundGui;

    public GameFrame() {
        this.setTitle(WINDOW_TITLE);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        this.setLocationRelativeTo(null);
        this.setResizable(true);
        this.setAlwaysOnTop(true);
        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());
        backgroundGui = new BackgroundGui();
        this.setContentPane(backgroundGui);

        this.setVisible(true);
    }

    public void restart(){
        this.addKeyListener(new Keyboard());
        backgroundGui = new BackgroundGui();
        this.setContentPane(backgroundGui);
        this.getContentPane().revalidate();
        this.getContentPane().repaint();
    }

    public BackgroundGui getBackgroundGui(){
        return this.backgroundGui;
    }

    public void setBackgroundGui(BackgroundGui backgroundGui){
        this.backgroundGui = backgroundGui;
    }

    @Override
    public JPanel getContentPane(){ return getBackgroundGui(); }

    public void shiftBackgorundToRight(){
        if (backgroundGui.getxPos() == -1) {
            backgroundGui.setxPos(0);
            backgroundGui.setBackground1PosX(-50);
            backgroundGui.setBackground2PosX(750);
        }
        backgroundGui.setMov(1);
    }

    public void shiftBackgorundToLeft() {
        if (backgroundGui.getxPos() == 4601) {
            backgroundGui.setxPos(4600);
            backgroundGui.setBackground1PosX(-50);
            backgroundGui.setBackground2PosX(750);
        }
        getBackgroundGui().setMov(-1);
    }

    public void stopShift(){
        backgroundGui.setMov(0);
    }

}
