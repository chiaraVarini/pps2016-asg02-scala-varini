package game.view.character;

import game.controller.Controller;
import utils.Res;
import utils.Utils;

import java.awt.*;

/**
 * Created by chiaravarini on 17/03/17.
 */
public class MarioGui implements GuiCharacter {

    private static final int MARIO_FREQUENCY = 25;
    private static final int JUMPING_LIMIT = 42;
    private static final String NAME_IMAGE = Res.IMGP_CHARACTER_MARIO();

    private int jumpingExtent;

    @Override
    public Image getImage() {
        return Utils.getImage(NAME_IMAGE);
    }

    @Override
    public Image deadImage() {
        return Utils.getImage(NAME_IMAGE);
    }


    @Override
    public Image walk(boolean moving, boolean toRight, int counter) {

        boolean justFrequency = counter % MARIO_FREQUENCY == 0;

        String str = Res.IMG_BASE() + NAME_IMAGE +
                (!moving ||justFrequency ? Res.IMGP_STATUS_ACTIVE() : Res.IMGP_STATUS_NORMAL()) +
                (toRight ? Res.IMGP_DIRECTION_DX() : Res.IMGP_DIRECTION_SX()) +
                Res.IMG_EXT();

        return Utils.getImage(str);
    }

    @Override
    public Image doJump() {

        String str;
        int height = Controller.getMainCharacterDimension("height");
        int yPosition = Controller.getMainCharacterPositon("y");
        boolean toRight = Controller.mainCharacterToRight();

        this.jumpingExtent++;

        if (this.jumpingExtent < JUMPING_LIMIT) {

            if (yPosition > Controller.getHeightLimit()) {
                Controller.setMainCharacterPosition("y", yPosition - 4);
            } else {
                this.jumpingExtent = JUMPING_LIMIT;
            }
            str = toRight ? Res.IMG_MARIO_SUPER_DX() : Res.IMG_MARIO_SUPER_SX();

        } else if (yPosition + height < Controller.getFloorOffsetY()) {
            Controller.setMainCharacterPosition( "y", yPosition + 1);
            str = toRight ? Res.IMG_MARIO_SUPER_DX() : Res.IMG_MARIO_SUPER_SX();
        } else {
            str = toRight ? Res.IMG_MARIO_ACTIVE_DX() : Res.IMG_MARIO_ACTIVE_SX();

            Controller.setMainCharacterJump(false);
            this.jumpingExtent = 0;
        }

        return Utils.getImage(str);
    }

    @Override
    public int getDeadOffset() {
        return 0;
    }

}
