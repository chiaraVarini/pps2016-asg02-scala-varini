package game.view.character;

import java.awt.*;

/**
 * Represent a GUI for generic Charcater
 * Created by chiaravarini on 17/03/17.
 */
public interface GuiCharacter {


    Image walk(boolean moving, boolean toRight, int counter);
    Image getImage();
    Image deadImage();
    Image doJump();

    int getDeadOffset();


}
