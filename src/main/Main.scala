package main

import javafx.embed.swing.JFXPanel

import game.controller.Controller

object Main {

  def main(args: Array[String]) {

    var fxPanel = new JFXPanel()
    Controller.startGame()
  }
}